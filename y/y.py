#Y type? v0.5
#made by Vlad C
#uses python 2.7 won't work in python 3
#avaliable under GPL v3
#typing game
#del some char
from y_classes import *
try: # getch function is from stack overflow
    import tty, termios
except ImportError:
    # Probably Windows.
    try:
        import msvcrt
    except ImportError:
        # FIXME what to do on other platforms?
        # Just give up here.
        raise ImportError('getch not available')
    else:
        getch = msvcrt.getch
else:
    def getch():
        """getch() -> key character

        Read a single keypress from stdin and return the resulting character. 
        Nothing is echoed to the console. This call will block if a keypress 
        is not already available, but will not wait for Enter to be pressed. 

        If the pressed key was a modifier key, nothing will be detected; if
        it were a special function key, it may return the first character of
        of an escape sequence, leaving additional characters in the buffer.
        """
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

key=0

first=True
in_menu=False
in_game=False
in_results=False
decision=0
task=''
task2=''
step=0

while key!=27:

    if first==True:
        GameMenu=Menu(11,True,True,True,True,False,0) #default settings length,cap,num,special_symbols,time_limit,display_speed_time,spaced
        GameMenu.display()
        booted=True
        in_menu=True

    key=ord(getch()) #always waits for input!

    if in_menu==True:
        if first==True:
            first=False
            if booted==False:
                GameMenu.display()
            else:
                booted=False
        #now going through options in menu start,exit,help,options
        if key==115: #start
            
            #go to game
            task=generator(GameMenu)
            in_menu=False
            in_game=True
            task_length=len(task)
        if key==111: #o
            GameMenu.options()
        if key==104: #h
            GameMenu.help()
    if in_game==True:
        try:
            task2,in_results=control(task,task_length,key,step)
            task=task2
        except:
           print("crap",in_results)
           in_results=True
    if in_results==True:
        decision= resolve(key)
        if decision==1:
            print('hi')
            in_results=False
            task=[]
            task2=[]
            task=generator(GameMenu)
            print task
            in_game=True
            task_length=len(task)
        if decision==2:
            GameMenu.display()
            in_menu=True
            in_results=False
        decision=0
    

        