#Y type? v0.5
#made by Vlad C
#uses python 2.7 won't work in python 3
#avaliable under GPL v3
#typing game
import os #for cls use: os.system('cls' if os.name=='nt' else 'clear')
import random #for gen
import time #for speed calc

errors=0


class Menu():
    #game menu
    def __init__(self,length,cap,num,normal,special_symbols,display_speed_time,spaced):
        self.length=length
        self.cap=cap
        self.num=num
        self.normal=normal
        self.special_symbols=special_symbols
        self.display_speed_time=display_speed_time
        self.spaced=spaced #if 0 spaces are random. any other setting will cause the space apppear every x char

    def display(self): 
        #intro
        print("\n")
        print("Y the tYping game avaliable under GPL v3!!! \n")
        print ("(s)tart \n")
        print ("exit (esc)\n")
        print ("(h)elp\n")
        print ("(o)ptions\n")
    
    def help(self): 
        #help
        print("\n")
        print ("go to options to enable disable settings!\n")
        print ("once the game starts type what you see, \n if you make a typeo you will have to type it again")
        print ("then results will be shown\n")
        print ("press esc to exit the game at any time \n")
        print ("good luck!")

    def options(self):
        #by tweaking these options list of avaliable char is changed and all other settings are passed to generator
        print ('\n')
        print ('use capitalized letter to increase setting or normal to decrease \n')
        print("length L/l(1) K/k(10) current: %d \n")
        print(" cap C/c current:%r \n")
        print("num N/n %r\n")
        print("lower-case symbols(aka letters) S/s %r \n")
        print("special symbols Y/y %r \n")
        print("display speed and time D/d %r\n")
        print("spacing A/a (1) Q/q(5)%d\n")

    def lengthen(self,number):
        length+=number
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def capitalize(self):
        if cap==True:
            cap=False
        else:
            cap=True
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def numbers(self):
        if num==True:
            num=False
        else:
            num=True
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def lower_case(self):
        if normal==True:
            normal=False
        else:
            normal=True
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def special(self):
        if special_symbols==True:
            special_symbols=False
        else:
            special_symbols=True
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def speed_time(self):
        if display_speed_time==True:
            display_speed_time=False
        else:
            display_speed_time=True
        os.system('cls' if os.name=='nt' else 'clear')
        options()

    def space(self,number):
        spacing+=number
        os.system('cls' if os.name=='nt' else 'clear')
        options()



def scores(key,errors,length,elapsed_time):
    global in_results
    global first
    in_results=True
    in_game=False
    first=True
    elapsed_time-=time.time()
    elapsed_time*=-1
    print ('time: %s speed: %s %%error %s \n') %(elapsed_time,length/elapsed_time*60,errors/float(length))
    print ('(r)estart or (m)enu or esc to exit \n')
    errors=0


def generator(menu_object):
    global started
    #generates and prints list
    #to add later: length,cap,num,special_symbols,display_speed_time,spaced
    os.system('cls' if os.name=='nt' else 'clear')
    seed=open("key.txt",'r')
    seeds=seed.read()
    seed.close()
    seeds=seeds.split(',')
    for idx in xrange(len(seeds)-1):
        try:
            if seeds[idx]=='':
                del seeds[idx]
        except:
            i=0
    task=[]
    #print seeds
    for size in xrange(menu_object.length):
        chosen=random.randint(0,len(seeds)-1)
        task.append(seeds[chosen])
    started=time.time()
    task[-1]=32
    tasked=''.join(chr(int(i)) for i in task)
    
    return tasked

def control(task,length,user_input,step):
    #checks current char vs user input and acts upon it
    global errors
    
    if len(task)==1:
        global started
        #print('catch')
        scores(user_input,errors,length,started)
        return ' ',True
    if length>step:
        
        if ord(task[step])==user_input:
            
            #delete the symbol from list
            task=task[1::]
            step+=1
            os.system('cls' if os.name=='nt' else 'clear')
            print (task)
            return task,False
        else:
            os.system('cls' if os.name=='nt' else 'clear')
            print (task)
            print ord(task[step])
            errors+=1
            return task,False
    
    else:
        global started
        scores(user_input,errors,length,started)

    


def resolve(key):
    print ('restart or menu')
    if key==114:
        #go to game
        return 1
    
    elif key==109:
        return 2
           